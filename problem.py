def word_list(current_word, output):
    word_list = []
    for i, (c1, c2) in enumerate(zip(current_word, output)):
        if c1 != c2:
            new_word = current_word[:i] + c2 + current_word[i + 1:]
            word_list.append(new_word)
    return word_list 

def word_transform(input: str, output: str):
    if input == output:
        return [input]
    
    with open("words.txt", "r") as file:
        dict_words = [line.strip() for line in file.readlines()]
    
    output_list = [input]
    current_word = input
    words_list = []
    
    
    while(current_word != output):
        words_list = word_list(current_word, output)
        #print(words_list)
        
        for word in words_list:
            if word not in output_list and word in dict_words:
                print(word)
                output_list.append(word)
                #print(output_list)
                current_word = word
                break
            
    return output_list

print(word_transform('CLAY', 'GOLD'))
print(word_transform('PARK', 'RIDE'))

